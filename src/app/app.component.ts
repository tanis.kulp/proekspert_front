import { Component, OnInit, OnDestroy } from '@angular/core';
import { DTO } from './core/DTO.model';
import { DTOService } from './core/DTO.services';
import {HttpErrorResponse} from '@angular/common/http';
import { AlertsService } from 'angular-alert-module';
import {timer} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  
    dto: DTO;
    dtoResponse: any;
    responseReceived = false;
    private timer: any;
    
    
    constructor(private dtoService: DTOService, private alerts: AlertsService){
   
    }

    ngOnInit() {
      this.timer = timer(0, 5000);
      this.timer.subscribe(() => this.makeRequest());
    }

    ngOnDestroy(): void {
      this.timer.unsubscribe();
    }

    makeRequest() {
      this.responseReceived = false;
      this.dtoService.getData().subscribe(
          response => {
            this.dtoResponse = response;
            this.dto = this.dtoResponse['body'];
            this.responseReceived = true;
          },
          (res: HttpErrorResponse) => this.onError(res.message));
    }

    private onError(errorMessage: string) {
      this.alerts.setMessage(errorMessage, 'error');
    }
}
