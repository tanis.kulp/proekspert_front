import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';

import {IDTO} from './DTO.model';

@Injectable({providedIn: 'root'})
export class DTOService {
    private resourceUrl = 'http://localhost:56666/api/values';

    constructor(private http: HttpClient) {
    }

    getData(): Observable<HttpResponse<IDTO>> {

        return this.http.get<IDTO>(this.resourceUrl, {observe: 'response'});
    }
}