export interface IDTO {
    dataReceived?: boolean;
    source?: string;
    active?: string;
    phone?: string;
    xlService?: string;
    language?: string;
    xlLanguage?: string;
    end?: string;
    xlActivation?: string;
    xlEnd?: string;
    overrideList?: string;
    phoneNumbers?: string[];
    names?: string[];
}

export class DTO implements IDTO {
    constructor(
        public dataReceived?: boolean,
        public source?: string,
        public active?: string,
        public phone?: string,
        public xlService?: string,
        public language?: string,
        public xlLanguage?: string,
        public end?: string,
        public xlActivation?: string,
        public xlEnd?: string,
        public overrideList?: string,
        public phoneNumbers?: string[],
        public names?: string[]
    ) {
        this.dataReceived = dataReceived ? dataReceived : null;
        this.source = source ? source : null;
        this.active = active ? active : null;
        this.phone = phone ? phone : null;
        this.xlService = xlService ? xlService : null;
        this.language = language ? language : null;
        this.xlLanguage = xlLanguage ? xlLanguage : null;
        this.end = end ? end : null;
        this.xlActivation = xlActivation ? xlActivation : null;
        this.xlEnd = xlEnd ? xlEnd : null;
        this.overrideList = overrideList ? overrideList : null;
        this.phoneNumbers = phoneNumbers ? phoneNumbers : null;
        this.names = names ? names : null;
    }
}
